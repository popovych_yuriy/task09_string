package com.epam.bigtask.model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BusinessLogic implements Model {
    private final String file = "src/main/resources/fain.txt";
    private String text;
    private String [] sentences;
    private String [] words;

    public BusinessLogic() {
        setText();
        splitSentences();
        splitWords();
    }

    private void setText() {
        try (Stream<String> stream = Files.lines(Paths.get(file))){
            text = stream.collect(Collectors.joining());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getText() {
        return text;
    }

    private void splitSentences(){
        sentences = text.split("\\. |\\.|\\? |\\?|\\! |\\!");
    }

    private void splitWords(){
        words = text.toLowerCase()
                .replaceAll("\\d|\\#|\\-|\\“|\\”", "")
                .split(" and | is | to | of | the | a |\\? |\\! |\\: |\\) |\\ \\(|\\ – |\\, |\\ |\\. |\\.");
    }

    @Override
    public String popularWord(){
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            Integer count = map.get(word);
            map.put(word, count != null ? count+1 : 0);
        }

        String popular = Collections.max(map.entrySet(),
                new Comparator<Map.Entry<String, Integer>>() {
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        return o1.getValue().compareTo(o2.getValue());
                    }
                }).getKey();
        return popular;
    }

    @Override
    public int sentencesWithPopularWord(){
        String popular = popularWord();
        int counter = 0;
        for (String sentence: sentences) {
            if(sentence.toLowerCase().contains(popular.toLowerCase())){
                counter++;
            }
        }
        return counter;
    }

    @Override
    public String[] sortByNumberOfWords(){
        String [] result = sentences;
        Arrays.sort(result, (a, b) -> countWords(a) < countWords(b) ? -1 : countWords(a) == countWords(b) ? 0 : 1);
        return result;
    }

    private int countWords(String line){
        if (line == null || line.isEmpty()) {
            return 0;
        }
        String[] words = line.split("\\s+");
        return words.length;
    }

    private List<String> splitSentence (String sentence){
        return new ArrayList<>(Arrays.asList(sentence.toLowerCase()
                .split(" and | is | to | of | the | a |\\? |\\! |\\: |\\) |\\ \\(|\\ – |\\, |\\ |\\. |\\.")));
    }

    @Override
    public String findUnique(){
        String [] tmp = sentences;
        String firstSentence = tmp[0];
        tmp[0] = "";
        System.out.println(firstSentence);
        List<String> firstWords = splitSentence(firstSentence);
        boolean occur = false;
        for (String word : firstWords) {
            for (String sentence : tmp) {
                if(sentence.toLowerCase().contains(word.toLowerCase())){
                    occur = true;
                }
            }
            if(occur == false){
                return word;
            }
            occur = false;
        }
        return null;
    }

    private List <String> findInterrogative(){
        List <String> interrogative = new ArrayList<>();
        Pattern pattern = Pattern.compile("([^.?!]*)\\?");
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()) {
            interrogative.add(matcher.group());
        }
        return interrogative;
    }

    @Override
    public Set<String> pullFromInterrogative(int length){
        List<String> interrogative = findInterrogative();
        Set<String> interWords = new HashSet<>();

        for (int i = 0; i < interrogative.size(); i++) {
            for (String word : splitSentence(interrogative.get(i))) {
                if(word.length() == length){
                    interWords.add(word);
                }
            }
        }
        return interWords;
    }

    @Override
    public String [] switchVowelLongest(){
        String [] tmpSentences = this.sentences;
        for (int i = 0; i < tmpSentences.length; i++) {
            String sentence = tmpSentences[i];
            List<String> tmpWords = splitSentence(sentence);
            int biggestIndex = 0;
            for (int j = 0; j < (tmpWords.size()-1); j++) {
                if(tmpWords.get(j).length() > tmpWords.get(j+1).length()){
                    biggestIndex = j;
                }
            }
            int vowelIndex = 0;
            for (int j = 0; j < tmpWords.size(); j++) {
                if(tmpWords.get(j).toLowerCase().matches("(?i)^[aeiouy].*$")){
                    vowelIndex = j;
                }
            }
            String tmp = tmpWords.get(biggestIndex);
            tmpWords.add(biggestIndex, tmpWords.get(vowelIndex));
            tmpWords.add(vowelIndex, tmp);
        }
        return tmpSentences;
    }

    @Override
    public List<String> printAlphabetic(){
        Set<String> tmp = new HashSet<>(Arrays.asList(words));
        List <String> result = new ArrayList<>(tmp);
        Collections.sort(result);
        return result;
    }

    @Override
    public List<String> sortVowelsPercentage(){
        Set<String> tmp = new HashSet<>(Arrays.asList(words));
        List <String> result = new ArrayList<>(tmp);
        Collections.sort(result);

        Collections.sort(result, new Comparator<String>() {
            @Override
            public int compare(String c1, String c2) {
                return Double.compare(countPercentageOfVowelLetter(c1), countPercentageOfVowelLetter(c2));
            }
        });
        return result;
    }

    private double countPercentageOfVowelLetter(String word) {
        double numberVowels = word
                .replaceAll("[^aeiou]", "")
                .length();
        return (numberVowels * 100) / word.length();
    }

    @Override
    public List<String> sortFirstConsonant() {
        Pattern pattern = Pattern.compile("^[aeoiu]");
        Set<String> tmpWords = new HashSet<>(Arrays.asList(words));
        List<String> result = new ArrayList<>();
        for (String s : tmpWords) {
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                result.add(s);
            }
        }
        return result;
    }
}
