package com.epam.bigtask.model;

import java.util.List;
import java.util.Set;

public interface Model {
    String getText();

    String popularWord();

    int sentencesWithPopularWord();

    String[] sortByNumberOfWords();

    String findUnique();

    Set<String> pullFromInterrogative(int length);

    String [] switchVowelLongest();

    List<String> printAlphabetic();

    List<String> sortVowelsPercentage();

    List<String> sortFirstConsonant();
}
