package com.epam.bigtask.controller;

import com.epam.bigtask.model.BusinessLogic;
import com.epam.bigtask.model.Model;

import java.util.List;
import java.util.Set;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public String getText() {
        return model.getText();
    }

    @Override
    public String popularWord() {
        return model.popularWord();
    }

    @Override
    public int sentencesWithPopularWord() {
        return model.sentencesWithPopularWord();
    }

    @Override
    public String[] sortByNumberOfWords() {
        return model.sortByNumberOfWords();
    }

    @Override
    public String findUnique() {
        return model.findUnique();
    }

    @Override
    public Set<String> pullFromInterrogative(int length) {
        return model.pullFromInterrogative(length);
    }

    @Override
    public String[] switchVowelLongest() {
        return model.switchVowelLongest();
    }

    @Override
    public List<String> printAlphabetic() {
        return model.printAlphabetic();
    }

    @Override
    public List<String> sortVowelsPercentage() {
        return model.sortVowelsPercentage();
    }

    @Override
    public List<String> sortFirstConsonant() {
        return model.sortFirstConsonant();
    }


}