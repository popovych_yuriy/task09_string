package com.epam.bigtask;

import com.epam.bigtask.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
