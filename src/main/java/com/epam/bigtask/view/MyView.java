package com.epam.bigtask.view;

import com.epam.bigtask.controller.Controller;
import com.epam.bigtask.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.*;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("0", "  0 - print text");
        menu.put("1", "  1 - Find the largest number of text sentences that have the same words.");
        menu.put("2", "  2 - Print sentences by increasing number of words.");
        menu.put("3", "  3 - Find a word in the first sentence that is not found in any of the other sentences.");
        menu.put("4", "  4 - In all interrogative sentences find without repetition words of a given length.");
        menu.put("5", "  5 - In each sentence replace the first word that begins with vowel to the longest word");
        menu.put("6", "  6 - Print the words of the text in alphabetical order");
        menu.put("7", "  7 - Sort words in ascending percentage of vowels");
        menu.put("8", "  8 - Sort words starting with vowels by the first consonant");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("0", this::printText);
        methodsMenu.put("1", this::mostPopular);
        methodsMenu.put("2", this::sort);
        methodsMenu.put("3", this::findUnique);
        methodsMenu.put("4", this::findInterrogative);
        methodsMenu.put("5", this::switchVowelLongest);
        methodsMenu.put("6", this::printAlphabetic);
        methodsMenu.put("7", this::sortVowelsPercentage);
        methodsMenu.put("8", this::sortFirstConsonant);
    }

    private void printText() {
        logger.info(controller.getText());
    }

    private void mostPopular() {
        String popular = controller.popularWord();
        int quantity = controller.sentencesWithPopularWord();
        logger.info("Most popular word is: " + popular + ", it occurs in " + quantity + " sentences.");
    }

    private void sort() {
        for (String line : controller.sortByNumberOfWords()) {
            logger.info(line);
        }
    }

    private void findUnique() {
        String word = controller.findUnique();
        if(word != null){
            logger.info("Word in the first sentence that is not found in any of the other sentences: " + word);
        }else {
            logger.info("There are no such words.");
        }
    }

    private void findInterrogative() {
        Set<String> words = controller.pullFromInterrogative(5);
        logger.info("Unique words in interrogative questions: ");
        logger.info(words.toString());
    }

    private void switchVowelLongest() {
        logger.info("In each sentence replace the first word that begins with vowel to the longest word:");
        for (String line : controller.switchVowelLongest()) {
            logger.info(line.toString());
        }
    }

    private void printAlphabetic() {
        logger.info("Print the words of the text in alphabetical order:");
        for (String line : controller.printAlphabetic()) {
            logger.info(line.toString());
        }
    }

    private void sortVowelsPercentage() {
        logger.info("Sort words in ascending percentage of vowels:");
        for (String line : controller.sortVowelsPercentage()) {
            logger.info(line.toString());
        }
    }

    private void sortFirstConsonant() {
        logger.info("Sort words in ascending percentage of vowels:");
        for (String line : controller.sortFirstConsonant()) {
            logger.info(line.toString());
        }


    }



    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}
