package com.epam.bigtask.view;

@FunctionalInterface
public interface Printable {

    void print();
}
