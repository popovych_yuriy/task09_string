package com.epam.tasks;

import com.epam.tasks.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
