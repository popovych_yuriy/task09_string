package com.epam.tasks.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BusinessLogic implements Model {
    StringUtils stringUtils;

    public BusinessLogic() {
        stringUtils = new StringUtils();
    }

    @Override
    public <T> String concatenate(T... objects) {
        return stringUtils.concatenate(objects);
    }

    @Override
    public boolean testRegEx(String line) {
        Pattern pattern = Pattern.compile("[A-Z].*\\.");
        Matcher matcher = pattern.matcher(line);
        return matcher.matches();
    }

    @Override
    public String[] split(String line) {
        return line.split("(the)|(you)");
    }

    @Override
    public String replace(String line) {
        return line.replaceAll("[aoueiAOUEI]", "_");
    }
}
