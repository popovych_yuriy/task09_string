package com.epam.tasks.model;

public interface Model {
    <T> String concatenate(T... objects);

    boolean testRegEx(String line);

    String[] split(String line);

    String replace(String line);
}
