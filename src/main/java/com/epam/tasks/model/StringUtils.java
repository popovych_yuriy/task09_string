package com.epam.tasks.model;

public class StringUtils {
    public <T> String concatenate(T... objects) {
        StringBuilder stringBuilder = new StringBuilder();
        for (T object : objects) {
            stringBuilder.append(object.toString()).append(" ");
        }
        return stringBuilder.toString();
    }
}
