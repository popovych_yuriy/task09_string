package com.epam.tasks.view;

@FunctionalInterface
public interface Printable {

    void print();
}
