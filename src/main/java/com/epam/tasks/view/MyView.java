package com.epam.tasks.view;

import com.epam.tasks.controller.Controller;
import com.epam.tasks.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private ResourceBundle bundle;
    private Locale locale;

    public MyView() {
        controller = new ControllerImpl();
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::menuUkrainian);
        methodsMenu.put("3", this::menuEnglish);
        methodsMenu.put("4", this::menuEsperanto);
        methodsMenu.put("5", this::testRegEx);
        methodsMenu.put("6", this::testSplit);
        methodsMenu.put("7", this::testReplace);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));//1 - test StringUtils
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("Q", bundle.getString("Q"));//Q - exit
    }

    private void testStringUtils() {
        logger.info("Test StringUtils: " +
                controller.concatenate("Hello World!", new Integer(42), new Object()));
    }

    private void menuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void menuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void menuEsperanto() {
        locale = new Locale("eo");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void testRegEx() {
        String text = "Test Regex expression.";
        logger.info("Text for test: " + text);
        if (controller.testRegEx(text)) {
            logger.info("Yes, it begins with a capital letter and ends with a period");
        } else {
            logger.info("No, it not begins with a capital letter and/or ends with a period");
        }
    }

    private void testSplit() {
        String line = "Lost with you, in you, and without you";
        logger.info("Text for test: " + line);
        logger.info(Arrays.toString(controller.split(line)));
    }

    private void testReplace() {
        String line = "Replace all the vowels in some text with underscores";
        logger.info("Text for test: " + line);
        logger.info(controller.replace(line));
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}
