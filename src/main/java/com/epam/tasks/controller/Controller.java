package com.epam.tasks.controller;

public interface Controller {
    <T> String concatenate(T... objects);

    boolean testRegEx(String line);

    String[] split(String line);

    String replace(String line);
}
