package com.epam.tasks.controller;

import com.epam.tasks.model.BusinessLogic;
import com.epam.tasks.model.Model;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public <T> String concatenate(T... objects) {
        return model.concatenate(objects);
    }

    @Override
    public boolean testRegEx(String line) {
        return model.testRegEx(line);
    }

    @Override
    public String[] split(String line) {
        return model.split(line);
    }

    @Override
    public String replace(String line) {
        return model.replace(line);
    }
}